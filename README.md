# Avtur: jet fuel for AWS Golang Devops Tools 

This library provides a set of useful functions for organizations building devops tools
in Golang for AWS.

It is totally bound to our conventions at VF Corporation, but others may find it to be useful.

Roadmap:

- [x] discover id of VPC with tag: VpcType=baseline
- [ ] discover ids of subnets with tags: SubnetType={public|private|egress}
- [ ] deliver VPC id and subnet ids (above) as a single result structure 
- [ ] discover ids of security groups with tags: SecurityGroupType={ssh|http|}