package avtur

import (
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/aws"
)

const AWS_DEFAULT_REGION = "us-east-1"

func GetSession() (*session.Session) {
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(AWS_DEFAULT_REGION)}, // TODO
	)
	if (err != nil) {
		panic("Unable to build Session.  Check your AWS credentials.")
	}
	return sess
}

