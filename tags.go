package avtur

import (
	"fmt"
	"github.com/aws/aws-sdk-go/service/ec2"
	"github.com/aws/aws-sdk-go/aws"
)

func GetBaselineVpcId() string {
	s := GetSession()
	client := ec2.New(s)

	filters := []*ec2.Filter{
		{
			Name:   aws.String("tag-key"),
			Values: []*string{aws.String("VpcType")},
		},
		{
			Name:   aws.String("tag-value"),
			Values: []*string{aws.String("baseline")},
		},
	}

	input := &ec2.DescribeVpcsInput{
		Filters: filters,
	}
	req, resp := client.DescribeVpcsRequest(input)

	err := req.Send()
	if err == nil {
		fmt.Println(resp.GoString())
		if len(resp.Vpcs) == 0{
			panic("Failed to find VPC with tag: 'VpcType'='baseline'")
		}
		return aws.StringValue(resp.Vpcs[0].VpcId);
	} else {
		panic("Failed to find VPC with tag: 'VpcType'='baseline'")
	}

}
